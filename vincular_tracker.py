#! /usr/bin/python3.6
from pymongo import MongoClient
from bson import DBRef
import csv

# client = MongoClient("mongodb://mongodb-node01.infra.konker-iot.marathon.mesos,mongodb-node02.infra.konker-iot.marathon.mesos,mongodb-node03.infra.konker-iot.marathon.mesos:27017")
client = MongoClient("mongodb://10.1.3.74:27017")
db = client.tracking
db.authenticate("konkerAdmin", "HEHrY4reKYsWPKtL")



def link_cars_and_trackers():
    with open('files/link_cars_trackers.csv', mode='r') as origin:
        reader = csv.DictReader(origin)
        line_count = 0
        for row in reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1

            tracker_id = row['tracker']
            vin = row['vin']
            date = row['date']

            tracker = db.trackers.find_one({'trackerId': tracker_id})
            car = db.cars.find_one({'_id': vin})

            if bool(tracker):
                db.trackers.update_one(
                    {'_id': tracker['_id']},
                    {'$set': {'status': 'ON_THE_ROAD', 'vin': vin}}
                )
            if bool(car):
                db.cars.update_one(
                    {'_id': car['_id']},
                    {'$set': {'status': 'TRACKING', 'tracker': DBRef('trackers', tracker['_id'])}}
                )
            db.trackerStatusLog.insert_one({'_class': 'com.konkerlabs.kgb.tracker.domain.model.TrackerStatusLog',
                                            'trackerGuid': tracker['guid'],
                                            'vin': vin,
                                            'oldStatus': tracker['status'],
                                            'newStatus': 'ON_THE_ROAD',
                                            'modifiedDate': date,
                                            'modifiedBy': 'teste@teste.com.br',
                                            'organization': DBRef('organizations', 'toyota')
                                            })
            print('Veiculo {} vinculado ao tracker {}.'.format(vin, tracker_id))


def main():
    link_cars_and_trackers()


if __name__ == "__main__":
    main()
