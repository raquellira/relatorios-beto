#! /usr/bin/python3.6
from pymongo import MongoClient
from bson import DBRef
import datetime
import csv

client = MongoClient("mongodb://mongodb-node01.infra.konker-iot.marathon.mesos,mongodb-node02.infra.konker-iot.marathon.mesos,mongodb-node03.infra.konker-iot.marathon.mesos:27017")
db = client.tracking
db.authenticate("konkerAdmin", "HEHrY4reKYsWPKtL")


def data_parser(data):
    data += ' 12:30'
    format_str = '%d/%m/%Y %H:%M'
    new_date = datetime.datetime.strptime(data, format_str)
    ts = int(new_date.timestamp()) * 1000
    return ts


def insert_stages():
    with open('insert_stages.csv', mode='r') as origin:
        reader = csv.DictReader(origin)
        line_count = 0
        for row in reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            vin = row['vin']
            tracker_id = row['tracker']
            data = row['data']
            new_data = data_parser(data)
            tracker = db.trackers.find_one({"trackerId": tracker_id})
            if bool(tracker):
                db.trackerStatusLog.insert_one({'_class': 'com.konkerlabs.kgb.tracker.domain.model.TrackerStatusLog',
                                                'trackerGuid': tracker['guid'],
                                                'vin': vin,
                                                'oldStatus': 'READY',
                                                'newStatus': 'ON_THE_ROAD',
                                                'modifiedDate': new_data,
                                                'modifiedBy': 'teste@teste.com.br',
                                                'organization': DBRef('organizations', 'toyota')
                                                })
                print('Ajustado {}'.format(tracker_id))
            else:
                print('tracker {} inexistente'.format(tracker_id))


def main():
    insert_stages()


if __name__ == "__main__":
    main()
