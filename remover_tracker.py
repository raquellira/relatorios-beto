#! /usr/bin/python3.6
from pymongo import MongoClient
import time

# client = MongoClient("mongodb://mongodb-node01.infra.konker-iot.marathon.mesos,mongodb-node02.infra.konker-iot.marathon.mesos,mongodb-node03.infra.konker-iot.marathon.mesos:27017")
client = MongoClient("mongodb://10.1.3.74:27017")
db = client.tracking
db.authenticate("konkerAdmin", "HEHrY4reKYsWPKtL")


def remove_tracker(tracker_id):
    cursor = db.trackers.find({"trackerId": tracker_id})
    tracker = cursor.next()
    tracker['removedAt'] = int(time.time() * 1000)
    db.trackers_graveyard.insert_one(tracker)
    del tracker['removedAt']
    db.trackers.remove(tracker)
    return "Removed {}".format(tracker_id)


def main():
    # trackers_list = ['T0304', 'T0307', 'T0312']
    # for tracker in trackers_list:
    #     remove_tracker(tracker)
    remove_tracker('T0312')


if __name__ == "__main__":
    main()

