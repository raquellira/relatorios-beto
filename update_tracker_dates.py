#! /usr/bin/python27
from pymongo import MongoClient
import datetime
import csv

client = MongoClient("mongodb://mongodb-node01.infra.konker-iot.marathon.mesos,mongodb-node02.infra.konker-iot.marathon.mesos,mongodb-node03.infra.konker-iot.marathon.mesos:27017")
db = client.tracking
db.authenticate("konkerAdmin", "HEHrY4reKYsWPKtL")


def data_parser(data):
    data += ' 12:30'
    format_str = '%d/%m/%Y %H:%M'
    new_date = datetime.datetime.strptime(data, format_str)
    ts = int(new_date.timestamp()) * 1000
    return ts


def update_tracker_status_dates():
    with open('files/update_dates.csv', mode='r') as origin:
        reader = csv.DictReader(origin)
        line_count = 0
        for row in reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            tracker_id = row['tracker']
            vin = row['vin']
            status = row['status']
            data = row['data']
            new_data = data_parser(data)
            tracker = db.trackers.find_one({"trackerId": tracker_id})
            tracker_log = db.trackerStatusLog.find_one({"trackerGuid": tracker['guid'], "newStatus": status, "vin": vin})
            if bool(tracker_log):
                db.trackerStatusLog.update_one(
                    {'_id': tracker_log['_id']},
                    {
                        '$set': {'modifiedDate': new_data}
                    }
                )
            print(tracker_id)


def main():
    update_tracker_status_dates()


if __name__ == "__main__":
    main()
