#! /usr/bin/python3.6
from pymongo import MongoClient
from bson.json_util import dumps

client = MongoClient("mongodb://localhost:27017")
db = client.tracking


def get_all_collection_json_filtered(collection, field, query):
    cursor = ''
    if query is None:
        cursor = db[collection].find({})
    else:
        cursor = db[collection].find({field: query})
    print(dumps(cursor))


def main():
    get_all_collection_json_filtered('notificationLogs', None, None)


if __name__ == "__main__":
    main()
