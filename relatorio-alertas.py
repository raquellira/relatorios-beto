#! /usr/bin/python3.6
from pymongo import MongoClient
from bson import objectid
import time
import csv

client = MongoClient("mongodb://mongodb-node01.infra.konker-iot.marathon.mesos,mongodb-node02.infra.konker-iot.marathon.mesos,mongodb-node03.infra.konker-iot.marathon.mesos:27017")
db = client.tracking
db.authenticate("konkerAdmin", "HEHrY4reKYsWPKtL")


def relatorio_alertas():
    alerts = db.alertevents.find({"ts": {"$gt": 1538352000000}}, {"type": 1, "ts": 1, "tracker.$id": 1, "car.$id": 1, "_id": 1})
    with open('files/relatorio_alertas.csv', mode='w') as report:
        writer = csv.writer(report, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Tipo do Alerta', 'Horario', 'Tracker', 'Dealer', 'Vin'])
        for alert in alerts:
            tracker = db.trackers.find_one({"_id": objectid.ObjectId(alert['tracker']['$id'])})
            car = db.cars.find_one({'_id': alert['car']['$id']})
            if 'dealership' not in car:
                continue
            dealer = db.dealerships.find_one({'_id': objectid.ObjectId(car['dealership'].id)})
            ts = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime(alert['ts']/1000))
            print(alert['_id'])
            if None not in [alert, tracker, dealer, car]:
                writer.writerow([alert['type'], ts, tracker['trackerId'], dealer['name'], car['_id']])


def main():
    relatorio_alertas()


if __name__ == "__main__":
    main()
