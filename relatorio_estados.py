#! /usr/bin/python3.6
from pymongo import MongoClient
from bson import objectid
from numpy import int64
import time
import csv

# client = MongoClient("mongodb://mongodb-node01.infra.konker-iot.marathon.mesos,mongodb-node02.infra.konker-iot.marathon.mesos,mongodb-node03.infra.konker-iot.marathon.mesos:27017")
client = MongoClient("mongodb://10.1.3.74:27017")
db = client.tracking
db.authenticate("konkerAdmin", "HEHrY4reKYsWPKtL")


def relatorio_estados():
    tracker_ids = db.trackers.distinct("trackerId", {"organization.$id": "toyota"})
    with open('files/tdb_satus_change.csv', mode='w') as report:
        writer = csv.writer(report, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Tracker', 'Vin', 'Dealer', 'Status', 'Horário'])
        for tracker_id in tracker_ids:
            tracker = db.trackers.find_one({"trackerId": tracker_id})
            tracker_status = db.trackerStatusLog.find({"trackerGuid": tracker['guid']})
            for status in tracker_status:
                print(status['_id'])
                if status['newStatus'] in ['BATTERY_DEPLETED', 'RETIRED', 'UNDER_INVESTIGATION', 'LOST']:
                    continue
                vin = ''
                dealer = ''
                # print(status['modifiedDate'])
                modified_date0 = int64(status['modifiedDate'])
                modified_date1 = modified_date0/1000
                modified_date2 = time.localtime(modified_date1)
                ts = time.strftime("%a, %d %b %Y %H:%M:%S", modified_date2)
                if status['newStatus'] in ['ON_THE_ROAD', 'ARRIVED_AT_DEALERSHIP', 'READY_TO_UNINSTALL']:
                    vin = status['vin']
                    car = db.cars.find_one(vin)
                    if car is None:
                        vin = 'DELETED'
                        continue
                    if 'dealership' in car:
                        dealership = db.dealerships.find_one({'_id': objectid.ObjectId(car['dealership'].id)})
                        dealer = dealership['name']
                writer.writerow([tracker_id, vin, dealer, status['newStatus'], ts])


def main():
    relatorio_estados()


if __name__ == "__main__":
    main()
