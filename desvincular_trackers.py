#! /usr/bin/python3.6
from pymongo import MongoClient
from bson import DBRef
import datetime
import time

# client = MongoClient("mongodb://mongodb-node01.infra.konker-iot.marathon.mesos,mongodb-node02.infra.konker-iot.marathon.mesos,mongodb-node03.infra.konker-iot.marathon.mesos:27017")
client = MongoClient("mongodb://10.1.3.74:27017")
db = client.tracking
db.authenticate("konkerAdmin", "HEHrY4reKYsWPKtL")


def unlink_cars(trackers_list, tracker_new_status, car_status):
    for tracker_id in trackers_list:
        tracker = db.trackers.find_one({'trackerId': tracker_id})
        if bool(tracker):
            car = db.cars.find_one({'tracker': DBRef('trackers', tracker['_id'])})
            if bool(car):
                modified_date = int(time.mktime(datetime.datetime.now().timetuple()) * 1000)
                db.trackerStatusLog.insert_one({'_class': 'com.konkerlabs.kgb.tracker.domain.model.TrackerStatusLog',
                                                'trackerGuid': tracker['guid'],
                                                'vin': car['_id'],
                                                'oldStatus': tracker['status'],
                                                'newStatus': tracker_new_status,
                                                'modifiedDate': modified_date,
                                                'modifiedBy': 'teste@teste.com.br',
                                                'organization': DBRef('organizations', 'toyota')
                                                })
                db.cars.update_one(
                    {'_id': car['_id']},
                    {
                        '$set': {'status': car_status},
                        '$unset': {'tracker': ''}
                    }
                )
                db.trackers.update_one(
                    {'_id': tracker['_id']},
                    {
                        '$set': {'status': tracker_new_status},
                        '$unset': {'vin': ''}
                    }
                )
                print('tracker {} desassociado do veiculo {}'.format(tracker['_id'], car['_id']))
            else:
                print('tracker {} não associado a veiculo'.format(tracker['_id']))
        else:
            print('tracker {} não existe'.format(tracker['_id']))


def main():
    trackers_list = ['T0246', 'T0190', 'T0133', 'T0219']
    unlink_cars(trackers_list, 'RETURNING', 'DELIVERED')


if __name__ == "__main__":
    main()

"""
+--------------------------------------------------------------------------------+
|                                   Tracker Status                               |
+-------------------------------+-------------------------------------------+----+
| Codigo                        | Descrição                                 |  # |
+-------------------------------+-------------------------------------------+----+
| READY                         | "Pronto para instalação"                  | 01 |
| ON_THE_ROAD                   | "Em trânsito"                             | 02 |
| ARRIVED_AT_DEALERSHIP         | "Na concessionária"                       | 03 |
| READY_TO_UNINSTALL            | "Pronto para desinstalação"               | 04 |
| STATIONED_AT_THE_DEALERSHIP   | "Aguardando devolução pela concessionária | 05 |
| RETURNING                     | "Retornado a fábrica"                     | 06 |
| WAITING_FOR_CHECKIN           | "Aguardando entrada na fábrica"           | 07 |
| MAINTENANCE                   | "Em manutenção"                           | 08 |
| BATTERY_DEPLETED              | "Sem bateria"                             | 09 |
| RETIRED                       | "Afastado"                                | 10 |
| UNDER_INVESTIGATION           | "Sob investigação"                        | 11 |
+-------------------------------+-------------------------------------------+----+

+---------------------------------------------------------------------------+
|                               Car Status                                  |
+-----------+---------------------------+-----------------------------------+
| Codigo    |          Descrição        |               Info                |
+-----------+---------------------------+-----------------------------------+
| NEW       | "Novo"                    | CAR WITH NO INSTALLATION DETAILS  |
| PENDING   | "Pronto para Instalação"  | CAR WITH INSTALLATION DETAILS     |
| TRACKING  | "Rastreado"               | CAR WITH TRACKER                  |
| DELIVERED | "Entregue"                | CAR DELIVERED                     |
+-----------+---------------------------+-----------------------------------+
"""